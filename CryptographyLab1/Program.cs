﻿using System;
using CryptographyLab1.Numbers;

namespace CryptographyLab1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("// NUMBER 1 START...");
            Number1.Run();
            Console.WriteLine("// NUMBER 1 END...");

            Console.WriteLine();

            Console.WriteLine("// NUMBER 2 START...");
            Number2.Run();
            Console.WriteLine("// NUMBER 2 END...");

            Console.WriteLine();

            Console.WriteLine("// NUMBER 3 START...");
            Number3.Run();
            Console.WriteLine("// NUMBER 3 END...");

            Console.WriteLine();

            Console.WriteLine("// NUMBER 4 START...");
            Number4.Run();
            Console.WriteLine("// NUMBER 4 END...");

            Console.WriteLine();

            Console.WriteLine("// NUMBER 5 START...");
            Number5.Run();
            Console.WriteLine("// NUMBER 5 END...");

            Console.WriteLine();

            Console.WriteLine("// NUMBER 6 START...");
            Number6.Run();
            Console.WriteLine("// NUMBER 6 END...");

            Console.WriteLine();

            Console.WriteLine("// NUMBER 7 START...");
            Number7.Run();
            Console.WriteLine("// NUMBER 7 END...");

            Console.WriteLine();

            Console.WriteLine("// NUMBER 8 START...");
            Number8.Run();
            Console.WriteLine("// NUMBER 8 END...");

            Console.WriteLine();

            Console.WriteLine("// NUMBER 9 START...");
            Number9.Run();
            Console.WriteLine("// NUMBER 9 END...");
            Console.WriteLine();

            Console.WriteLine("// NUMBER 10 START...");
            Number10.Run();
            Console.WriteLine("// NUMBER 10 END...");

            Console.WriteLine();
            Console.WriteLine("// NUMBER 11 START...");
            Number11.Run();
            Console.WriteLine("// NUMBER 11 END...");

            Console.WriteLine();

            Console.WriteLine("// NUMBER 12 START...");
            Number12.Run();
            Console.WriteLine("// NUMBER 12 END...");

            Console.WriteLine();

            Console.WriteLine("// NUMBER 13 START...");
            Number13.Run();
            Console.WriteLine("// NUMBER 13 END...");

            Console.WriteLine();

            Console.WriteLine("// NUMBER 14 START...");
            Number14.Run();
            Console.WriteLine("// NUMBER 14 END...");

            Console.WriteLine();

            Console.ReadKey();
        }
    }
}