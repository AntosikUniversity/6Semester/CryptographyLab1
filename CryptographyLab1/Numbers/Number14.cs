﻿using System;

namespace CryptographyLab1.Numbers
{
    public static class Number14
    {
        public static void Run()
        {
            Console.WriteLine("Начальный текст - [10, 9, 8, 7, 6]");
            var message = new byte[] {10, 9, 8, 7, 6};

            Console.WriteLine($"Шифр RC4: (ключ - [1, 2, 3, 4, 5]");
            var key = new byte[] {1, 2, 3, 4, 5};

            Console.WriteLine();

            var rc4 = new RC4(key);
            var encodedWithRC4 = rc4.Decode(message);
            Console.Write($"Зашифрованный текст: ");
            foreach (var b in encodedWithRC4) Console.Write($"{b} ");
            Console.WriteLine();

            var rc4_2 = new RC4(key);
            var decodedWithRC4 = rc4_2.Encode(encodedWithRC4);
            Console.Write($"Дешифрованный текст: ");
            foreach (var b in decodedWithRC4) Console.Write($"{b} ");
            Console.WriteLine();
        }
    }

    public class RC4
    {
        private readonly byte[] S = new byte[256];
        private int x;
        private int y;

        public RC4(byte[] key)
        {
            init(key);
        }

        private void init(byte[] key)
        {
            var keyLength = key.Length;

            for (var i = 0; i < 256; i++) S[i] = (byte) i;

            var j = 0;
            for (var i = 0; i < 256; i++)
            {
                j = (j + S[i] + key[i % keyLength]) % 256;
                Swap(S, i, j);
            }
        }

        private byte keyItem()
        {
            x = (x + 1) % 256;
            y = (y + S[x]) % 256;

            Swap(S, x, y);

            return S[(S[x] + S[y]) % 256];
        }

        public byte[] Encode(byte[] data)
        {
            var cipher = new byte[data.Length];

            for (var m = 0; m < data.Length; m++) cipher[m] = (byte) (data[m] ^ keyItem());

            return cipher;
        }

        public byte[] Decode(byte[] data)
        {
            return Encode(data);
        }

        private static void Swap(byte[] array, int i, int j)
        {
            var temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
    }
}