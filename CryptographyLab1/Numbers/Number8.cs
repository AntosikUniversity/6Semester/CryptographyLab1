﻿using System;
using System.Collections;
using System.Linq;

namespace CryptographyLab1.Numbers
{
    // TODO: rework
    public static class Number8
    {
        public static int BitShuffle(int number, int[] shuffle)
        {
            var bitArray = new BitArray(new[] {number});
            var result = new BitArray(32);
            shuffle = shuffle.Reverse().ToArray();

            for (int i = 0, len = shuffle.Length; i < len; i++) result[i] = bitArray[shuffle[i]];

            return BitArrayToInt32(result);
        }

        public static int BitArrayToInt32(BitArray bitArray)
        {
            if (bitArray.Length > 32)
                throw new ArgumentException("Argument length shall be at most 32 bits.");

            var array = new int[1];
            bitArray.CopyTo(array, 0);

            return array[0];
        }

        public static void Run()
        {
            // Main
            Console.WriteLine("Введите 32-х разрядное целое число 𝑎 в двоичной системе счисления");
            var numberStr = Console.ReadLine();
            var number = 0;
            try
            {
                if (string.IsNullOrEmpty(numberStr)) throw new Exception("Incorrect number");
                number = Convert.ToInt32(numberStr, 2);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Invalid data! Error - {e.Message}");
                return;
            }

            var numberLen = numberStr.Length;

            Console.WriteLine($"Введенное число: {number}");

            var shuffle = new int[numberLen];

            for (var i = 0; i < numberLen; i++)
            {
                Console.WriteLine($"Введите новый номер бита #{i + 1}:");
                var newBitNumberStr = Console.ReadLine();
                if (!sbyte.TryParse(newBitNumberStr, out var newBitNumber) || newBitNumber > 32 || newBitNumber == 0)
                {
                    Console.WriteLine("Invalid data!");
                    return;
                }

                shuffle[i] = newBitNumber - 1;
            }

            Console.WriteLine();
            var shuffled = BitShuffle(number, shuffle);
            Console.WriteLine($"Новое число после перестановки - {shuffled} ({Convert.ToString(shuffled, 2)})");
        }
    }
}