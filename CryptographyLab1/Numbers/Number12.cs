﻿using System;
using System.Linq;

namespace CryptographyLab1.Numbers
{
    public static class Number12
    {
        public static void Run()
        {
            Console.WriteLine("Начальный текст - Hello World");
            var message = "Hello World";

            Console.WriteLine();
            Console.WriteLine($"Шифр Вернама: (ключ - \"Welcome too\")");
            var key = "Welcome too";

            var encodedWithVernam = VernamCipher.Encrypt(message, key);
            Console.WriteLine($"Зашифрованный текст - {encodedWithVernam}");
            Console.WriteLine($"Дешифрованный текст - {VernamCipher.Decrypt(encodedWithVernam, key)}");
            Console.WriteLine();

            var mess1 = "Hello World";
            var mess2 = "How is it  ";
            Console.WriteLine($"Начальный текст 1 - {mess1}");
            Console.WriteLine($"Начальный текст 2 - {mess2}");

            Console.WriteLine($"Шифр Вернама: (ключ - {key})");
            var messVernam1 = VernamCipher.Encrypt(mess1, key);
            var messVernam2 = VernamCipher.Encrypt(mess2, key);
            Console.WriteLine($"Зашифрованный текст 1 - {messVernam1}");
            Console.WriteLine($"Зашифрованный текст 2 - {messVernam2}");
            Console.WriteLine($"Взломанный текст - {VernamCipher.Hack(messVernam1, messVernam2)}");
        }
    }

    public static class VernamCipher
    {
        public static string Encrypt(string message, string key)
        {
            if (message.Length != key.Length) throw new ArgumentException("Lengths of message & key must be equal");

            return new string(
                message.Select(
                    (ch, i) => (char) (ch ^ key[i])
                ).ToArray()
            );
        }

        public static string Decrypt(string message, string key)
        {
            if (message.Length != key.Length) throw new ArgumentException("Lengths of message & key must be equal");

            return new string(
                message.Select(
                    (ch, i) => (char) (ch ^ key[i])
                ).ToArray()
            );
        }

        public static string Hack(string message1, string message2)
        {
            var len = message1.Length > message2.Length ? message2.Length : message1.Length;
            var result = new char[len];

            for (var i = 0; i < len; i++) result[i] = (char) (message1[i] ^ message2[i]);

            return new string(result);
        }
    }
}