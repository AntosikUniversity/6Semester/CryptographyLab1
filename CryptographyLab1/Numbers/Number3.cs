﻿using System;

namespace CryptographyLab1.Numbers
{
    public static class Number3
    {
        public static int SwapBytes(int number, sbyte iByteNumber, sbyte jByteNumber, sbyte n = 1)
        {
            // Move all bits of first set to rightmost side 
            var set1 = (number >> ((iByteNumber - 1) * 8)) & ((1 << 8) - 1);

            // Moce all bits of second set to rightmost side 
            var set2 = (number >> ((jByteNumber - 1) * 8)) & ((1 << 8) - 1);

            // XOR the two sets 
            var xor = set1 ^ set2;

            // Put the xor bits back to their original positions 
            xor = (xor << ((iByteNumber - 1) * 8)) | (xor << ((jByteNumber - 1) * 8));

            // XOR the 'xor' with the original number so that the two sets are swapped 
            var result = number ^ xor;

            return result;
        }

        public static void Run()
        {
            // Main
            Console.WriteLine("Введите 32-х разрядное целое число 𝑎 в двоичной системе счисления");
            var numberStr = Console.ReadLine();
            var number = 0;
            try
            {
                if (string.IsNullOrEmpty(numberStr)) throw new Exception("Incorrect number");
                number = Convert.ToInt32(numberStr, 2);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Invalid data! Error - {e.Message}");
                return;
            }

            Console.WriteLine($"Введенное число: {number}");

            // 3
            Console.WriteLine();
            Console.WriteLine("Введите номер бита i:");
            var iByteNumberStr = Console.ReadLine();
            if (!sbyte.TryParse(iByteNumberStr, out var iByteNumber) || iByteNumber > 4 || iByteNumber == 0)
            {
                Console.WriteLine("Invalid data!");
                return;
            }

            Console.WriteLine("Введите номер бита j:");
            var jByteNumberStr = Console.ReadLine();
            if (!sbyte.TryParse(jByteNumberStr, out var jByteNumber) || iByteNumber > 4 || jByteNumber == 0)
            {
                Console.WriteLine("Invalid data!");
                return;
            }

            Console.WriteLine(
                $"Поменяли байты с номерами {iByteNumber} и {jByteNumber}. Полученное число - {SwapBytes(number, iByteNumber, jByteNumber)}");
        }
    }
}