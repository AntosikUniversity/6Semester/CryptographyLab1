﻿using System;
using System.Collections;

namespace CryptographyLab1.Numbers
{
    public static class Number5
    {
        // TODO: rework
        public static int Pow2Number(int number)
        {
            var bitArray = new BitArray(new[] {number});
            for (var i = bitArray.Count - 1; i >= 0; i--)
            {
                if (bitArray[i])
                    return i;
            }

            return 0;
        }

        public static void Run()
        {
            // Main
            Console.WriteLine("Введите 32-х разрядное целое число 𝑎 в двоичной системе счисления");
            var numberStr = Console.ReadLine();
            var number = 0;
            try
            {
                if (string.IsNullOrEmpty(numberStr)) throw new Exception("Incorrect number");
                number = Convert.ToInt32(numberStr, 2);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Invalid data! Error - {e.Message}");
                return;
            }

            Console.WriteLine($"Введенное число: {number}");

            Console.WriteLine();
            var p = Pow2Number(number);
            Console.WriteLine($"Число p равно - {p}. {Math.Pow(2, p)} <= {number} <= {Math.Pow(2, p + 1)}");
        }
    }
}