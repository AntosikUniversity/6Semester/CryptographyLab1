﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CryptographyLab1.Numbers
{
    public static class Number13
    {
        public static void Run()
        {
            Console.WriteLine("DES Algorythm");
            Console.WriteLine("See http://page.math.tu-berlin.de/~kant/teaching/hess/krypto-ws2006/des.htm");

            Console.WriteLine();

            var msgHex = "0123456789ABCDEF";
            var msg = HEXtoBIT(msgHex);
            Console.WriteLine($"Начальный текст - {msgHex} = {msg}");

            var keyHex = "133457799BBCDFF1";
            var key = HEXtoBIT(keyHex);
            Console.WriteLine($"Ключ - {keyHex} = {key}");


            var result = DESBase.Encrypt(msg, key);
            var resultHex = BITtoHEX(result);

            Console.WriteLine($"Зашифрованный текст - {resultHex} = {result}");
        }

        public static string HEXtoBIT(string hex)
        {
            var result = string.Empty;

            int quotient = Math.DivRem(hex.Length, 2, out int remainder);
            var blocksCount = remainder == 0 ? quotient : quotient + 1;

            for (int index = 0, len = blocksCount ; index < len; index++)
            {
                var block = hex.Substring(index * 2, 2);
                result += Convert.ToString(Convert.ToInt32(block, 16), 2).PadLeft(8, '0').ToUpper();
            }

            return result.PadLeft(64, '0');
        }

        public static string BITtoHEX(string bit)
        {
            var result = string.Empty;

            int quotient = Math.DivRem(bit.Length, 8, out int remainder);
            var blocksCount = remainder == 0 ? quotient : quotient + 1;

            for (int index = 0, len = blocksCount; index < len; index++)
            {
                var block = bit.Substring(index * 8, 8);
                result += Convert.ToString(Convert.ToInt32(block, 2), 16).PadLeft(2, '0').ToUpper();
            }

            return result.PadLeft(16, '0');
        }
    }

    public static class DESBase
    {
        public enum EncryptionMode
        {
            ECB,
            CBC,
            OFB,
            CFB
        }

        public static string Encrypt(string message, string key)
        {
            var keys = GetKeys(key);
            var result = GetEncodedString(message, keys);

            return result;
        }

        #region Helpers

        private static string EncodeStringWithArray(string key, byte[] array)
        {
            var result = string.Empty;
            var len = array.Length;

            for (var i = 0; i < len; i++) result += key[array[i] - 1];

            return result;
        }

        #endregion

        #region Tables

        public static byte[] iterationShift1Indexes = {1, 2, 9, 16};

        public static byte[] PC1 =
        {
            57, 49, 41, 33, 25, 17, 9,
            1, 58, 50, 42, 34, 26, 18,
            10, 2, 59, 51, 43, 35, 27,
            19, 11, 3, 60, 52, 44, 36,
            63, 55, 47, 39, 31, 23, 15,
            7, 62, 54, 46, 38, 30, 22,
            14, 6, 61, 53, 45, 37, 29,
            21, 13, 5, 28, 20, 12, 4
        };

        public static byte[] PC2 =
        {
            14, 17, 11, 24, 1, 5,
            3, 28, 15, 6, 21, 10,
            23, 19, 12, 4, 26, 8,
            16, 7, 27, 20, 13, 2,
            41, 52, 31, 37, 47, 55,
            30, 40, 51, 45, 33, 48,
            44, 49, 39, 56, 34, 53,
            46, 42, 50, 36, 29, 32
        };

        public static byte[] IP =
        {
            58, 50, 42, 34, 26, 18, 10, 2,
            60, 52, 44, 36, 28, 20, 12, 4,
            62, 54, 46, 38, 30, 22, 14, 6,
            64, 56, 48, 40, 32, 24, 16, 8,
            57, 49, 41, 33, 25, 17, 9, 1,
            59, 51, 43, 35, 27, 19, 11, 3,
            61, 53, 45, 37, 29, 21, 13, 5,
            63, 55, 47, 39, 31, 23, 15, 7
        };

        public static byte[] IPInv =
        {
            40, 8, 48, 16, 56, 24, 64, 32,
            39, 7, 47, 15, 55, 23, 63, 31,
            38, 6, 46, 14, 54, 22, 62, 30,
            37, 5, 45, 13, 53, 21, 61, 29,
            36, 4, 44, 12, 52, 20, 60, 28,
            35, 3, 43, 11, 51, 19, 59, 27,
            34, 2, 42, 10, 50, 18, 58, 26,
            33, 1, 41, 9, 49, 17, 57, 25
        };

        public static byte[] E =
        {
            32, 1, 2, 3, 4, 5,
            4, 5, 6, 7, 8, 9,
            8, 9, 10, 11, 12, 13,
            12, 13, 14, 15, 16, 17,
            16, 17, 18, 19, 20, 21,
            20, 21, 22, 23, 24, 25,
            24, 25, 26, 27, 28, 29,
            28, 29, 30, 31, 32, 1
        };

        public static byte[] P =
        {
            16, 7, 20, 21,
            29, 12, 28, 17,
            1, 15, 23, 26,
            5, 18, 31, 10,
            2, 8, 24, 14,
            32, 27, 3, 9,
            19, 13, 30, 6,
            22, 11, 4, 25
        };

        public static byte[,] SBoxes =
        {
            {
                14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7,
                0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8,
                4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0,
                15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13
            },
            {
                15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10,
                3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5,
                0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15,
                13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9
            },
            {
                10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8,
                13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1,
                13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7,
                1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12
            },
            {
                7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15,
                13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9,
                10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4,
                3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14
            },
            {
                2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9,
                14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6,
                4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14,
                11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3
            },
            {
                12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11,
                10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8,
                9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6,
                4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13
            },
            {
                4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1,
                13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6,
                1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2,
                6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12
            },
            {
                13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7,
                1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2,
                7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8,
                2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11
            }
        };

        #endregion

        #region KeyManaging

        public static string[] GetKeys(string key)
        {
            var key56 = PC1Shuffle(key);
            var keyParts = SplitKey(key56);

            var cdPairs = new List<Tuple<string, string>>(17) {keyParts};
            for (byte i = 1; i < 17; i++)
            {
                cdPairs.Add(
                    new Tuple<string, string>(
                        Shift(cdPairs[i - 1].Item1, i),
                        Shift(cdPairs[i - 1].Item2, i)
                    )
                );
            }

            var keys = new string[17];
            for (byte i = 1; i < 17; i++) keys[i] = PC2Shuffle(cdPairs[i].Item1 + cdPairs[i].Item2);

            return keys;
        }

        private static string PC1Shuffle(string key)
        {
            return EncodeStringWithArray(key, PC1);
        }

        private static string PC2Shuffle(string key)
        {
            return EncodeStringWithArray(key, PC2);
        }

        private static Tuple<string, string> SplitKey(string key)
        {
            var centerIndex = key.Length / 2;

            var left = key.Substring(0, centerIndex);
            var right = key.Substring(centerIndex, centerIndex);

            return new Tuple<string, string>(left, right);
        }

        private static string Shift(string key, byte index)
        {
            var result = key;

            if (iterationShift1Indexes.Contains(index))
                result = LeftCycleShift(result, 1);
            else
                result = LeftCycleShift(result, 2);

            return result;
        }

        private static string LeftCycleShift(string key, byte bits)
        {
            var firstBit = key.Substring(0, bits);
            return key.Remove(0, bits) + firstBit;
        }

        private static string UniteBlocks(Tuple<string, string> parts)
        {
            return parts.Item1 + parts.Item2;
        }

        #endregion

        #region MessageManaging

        public static string GetEncodedString(string message, string[] keys)
        {
            var messageAfterIP = IPShuffle(message);
            var splittedIP = SplitKey(messageAfterIP);

            var lpParts = new List<Tuple<string, string>>(17) {splittedIP};

            // f function

            for (byte i = 1; i < 17; i++)
            {
                var left = lpParts[i - 1].Item1;
                var right = lpParts[i - 1].Item2;

                var encodedRight = EShuffle(right);
                var b = XOR(keys[i], encodedRight);

                var sBlockResult = string.Empty;
                for (var index = 0; index < 8; index++)
                {
                    var block = b.Substring(index * 6, 6);

                    var row = Convert.ToInt32(
                        new string(new[] {block.First(), block.Last()}), 2
                    );
                    var column = Convert.ToInt32(
                        block.Substring(1, 4), 2
                    );

                    var sBlock = GetSBlock(index, row, column);
                    sBlockResult += sBlock;
                }

                var afterP = PShuffle(sBlockResult);

                var newLeft = right;
                var newRight = XOR(left, afterP);

                lpParts.Add(
                    new Tuple<string, string>(
                        newLeft,
                        newRight
                    )
                );
            }

            var rlAfterIP = lpParts.Select(
                pair => IPInvShuffle(pair.Item2 + pair.Item1)
            );

            return rlAfterIP.Last();
        }

        private static string IPShuffle(string message)
        {
            return EncodeStringWithArray(message, IP);
        }

        private static string IPInvShuffle(string message)
        {
            return EncodeStringWithArray(message, IPInv);
        }

        private static string EShuffle(string message)
        {
            return EncodeStringWithArray(message, E);
        }

        public static string XOR(string first, string second)
        {
            var result = string.Empty;
            int firstLen = first.Length, secondLen = second.Length;

            if (firstLen < secondLen)
                first = first.PadLeft(secondLen, '0');
            else if (firstLen > secondLen)
                second = second.PadLeft(firstLen, '0');

            for (var i = 0; i < firstLen; i++) result += first[i] == second[i] ? '0' : '1';

            return result;
        }

        private static string GetSBlock(int block, int row, int column)
        {
            var item = Convert.ToString(SBoxes[block, row * 16 + column], 2);
            return item.PadLeft(4, '0');
        }

        private static string PShuffle(string message)
        {
            return EncodeStringWithArray(message, P);
        }

        #endregion
    }

    public static class DESEncryption
    {
        public static string Run(DESBase.EncryptionMode mode, string message, string key, string IV = "0")
        {
            switch (mode)
            {
                case DESBase.EncryptionMode.CBC: return DESEncryption.DES_CBC(message, key, IV);
                case DESBase.EncryptionMode.CFB: return DESEncryption.DES_CFB(message, key, IV);
                case DESBase.EncryptionMode.ECB: return DESEncryption.DES_ECB(message, key);
                case DESBase.EncryptionMode.OFB: return DESEncryption.DES_OFB(message, key, IV);
                default: throw new ArgumentException("Incorrect mode!");
            }
        }

        public static string DES_CBC(string message, string key, string IV)
        {
            var blocks = SplitToBlocks(message);

            var results = new List<string>();
            for (int i = 0, len = blocks.Length; i < len; i++)
            {
                results.Add(
                    DESBase.Encrypt(
                        DESBase.XOR(
                            i == 0 ? IV : results[i - 1],
                            blocks[i]
                        ),
                        key
                    )
                );
            }

            return String.Join("", results);
        }

        public static string DES_CFB(string message, string key, string IV)
        {
            var blocks = SplitToBlocks(message);

            var results = new List<string>();
            for (int i = 0, len = blocks.Length; i < len; i++)
            {
                results.Add(
                    DESBase.XOR(
                        DESBase.Encrypt(
                            i == 0 ? IV : results[i - 1],
                            key
                        ),
                        blocks[i]
                    )
                );
            }

            return String.Join("", results);
        }

        public static string DES_ECB(string message, string key)
        {
            return String.Join("",
                    SplitToBlocks(message)
                        .Select(block =>
                            DESBase.Encrypt(block, key)
                        )
                );
        }

        public static string DES_OFB(string message, string key, string IV)
        {
            var blocks = SplitToBlocks(message);

            var results = new List<string>();
            var resultsKeys = new List<string>() { IV };

            for (int i = 0, len = blocks.Length; i < len; i++)
            {
                var K = DESBase.Encrypt(
                    resultsKeys[i],
                    key
                );

                resultsKeys.Add(K);
                    
                results.Add(
                    DESBase.XOR(
                        K,
                        blocks[i]
                    )
                );
            }

            return String.Join("", results);
        }

        public static string[] SplitToBlocks(string message)
        {
            var result = new List<string>();

            int quotient = Math.DivRem(message.Length, 64, out int remainder);
            var blocksCount = remainder == 0 ? quotient : quotient + 1;

            for (int i = blocksCount - 1; i > 0; i--)
            {
                var block = message.Substring(i * 64, 64);
                result.Add(block.PadLeft(64, '0'));
            }

            return result.ToArray();
        }
    }
}