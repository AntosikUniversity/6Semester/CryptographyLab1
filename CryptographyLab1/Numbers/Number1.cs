﻿using System;

namespace CryptographyLab1.Numbers
{
    public static class Number1
    {
        public static int ShowBit(int number, sbyte bitNumber)
        {
            return (number & (1 << (bitNumber - 1))) >> (bitNumber - 1);
        }

        public static int ToggleBit(int number, sbyte bitNumber)
        {
            return number ^ (1 << (bitNumber - 1));
        }

        public static int SwapBits(int number, sbyte iBitNumber, sbyte jBitNumber, sbyte n = 1)
        {
            // Move all bits of first set to rightmost side 
            var set1 = (number >> (iBitNumber - 1)) & ((1 << n) - 1);

            // Moce all bits of second set to rightmost side 
            var set2 = (number >> (jBitNumber - 1)) & ((1 << n) - 1);

            // XOR the two sets 
            var xor = set1 ^ set2;

            // Put the xor bits back to their original positions 
            xor = (xor << (iBitNumber - 1)) | (xor << (jBitNumber - 1));

            // XOR the 'xor' with the original number so that the two sets are swapped 
            var result = number ^ xor;

            return result;
        }

        public static int NullBits(int number, sbyte bitNumber)
        {
            return (number >> bitNumber) << bitNumber;
        }

        public static void Run()
        {
            // Main
            Console.WriteLine("Введите 32-х разрядное целое число 𝑎 в двоичной системе счисления");
            var numberStr = Console.ReadLine();
            var number = 0;
            try
            {
                number = Convert.ToInt32(numberStr, 2);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Invalid data! Error - {e.Message}");
                return;
            }

            Console.WriteLine($"Введенное число: {number}");

            // 1, 2
            Console.WriteLine();
            Console.WriteLine("Введите номер бита:");
            var bitNumberStr = Console.ReadLine();
            if (!sbyte.TryParse(bitNumberStr, out var bitNumber))
            {
                Console.WriteLine("Invalid data!");
                return;
            }

            Console.WriteLine($"1. Значение бита - {ShowBit(number, bitNumber)}");
            Console.WriteLine($"2. После переключение бита, число равно - {ToggleBit(number, bitNumber)}");

            // 3
            Console.WriteLine();
            Console.WriteLine("Введите номер бита i:");
            var iBitNumberStr = Console.ReadLine();
            if (!sbyte.TryParse(iBitNumberStr, out var iBitNumber))
            {
                Console.WriteLine("Invalid data!");
                return;
            }

            Console.WriteLine("Введите номер бита j:");
            var jBitNumberStr = Console.ReadLine();
            if (!sbyte.TryParse(jBitNumberStr, out var jBitNumber))
            {
                Console.WriteLine("Invalid data!");
                return;
            }

            Console.WriteLine(
                $"3. Поменяли биты с номерами {iBitNumber} и {jBitNumber}. Полученное число - {SwapBits(number, iBitNumber, jBitNumber)}");

            // 4
            Console.WriteLine();
            Console.WriteLine("Введите количество бит, которое вы хотите обнулить:");
            var mBitNumberStr = Console.ReadLine();
            if (!sbyte.TryParse(mBitNumberStr, out var mBitNumber))
            {
                Console.WriteLine("Invalid data!");
                return;
            }

            Console.WriteLine(
                $"4. После обнуления {mBitNumber} младших бит число равно - {NullBits(number, mBitNumber)}");
        }
    }
}