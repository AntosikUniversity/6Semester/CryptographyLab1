﻿using System;
using System.Linq;

namespace CryptographyLab1.Numbers
{
    public static class Number10
    {
        public static void Run()
        {
            // Main
            var str = "Relative frequencies of letters in the English language";
            Console.WriteLine($"Начальный текст - {str}");
            Console.WriteLine();
            var randomKey = Helpers.Random.Next(0, 25);
            Console.WriteLine($"Шифр сдвига: (ключ - {randomKey})");
            var encodedWithShift = ShiftCipher.Encrypt(str, randomKey);
            Console.WriteLine($"Зашифрованный текст - {encodedWithShift}");
            Console.WriteLine();
            var hackedShift = ShiftCipher.Hack(str);
            Console.WriteLine($"Взломанный текст - {hackedShift}");
        }
    }

    public static partial class ShiftCipher
    {
        public static string Hack(string data)
        {
            var mostFrequentChar = data
                .GroupBy(ch => ch)
                .OrderByDescending(group => group.Count())
                .First()
                .Key;
            var topFrequnecyChar = Helpers.GetCharInLetterFrequnecyPos(1);

            var key = topFrequnecyChar - mostFrequentChar;

            return Decrypt(data, key);
        }
    }
}