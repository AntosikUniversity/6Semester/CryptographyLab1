﻿using System;

namespace CryptographyLab1.Numbers
{
    public static class Number2
    {
        public static int ConnectParts(int number, sbyte length, sbyte count)
        {
            return ((number >> (length - count)) << count) | (number & ~(~0 << count));
        }

        public static int GetBitsBetween(int number, sbyte length, sbyte count)
        {
            return (number >> count) & ~(~0 << (length - 2 * count));
        }

        public static void Run()
        {
            // Main
            Console.WriteLine("Введите 32-х разрядное целое число 𝑎 в двоичной системе счисления");
            var numberStr = Console.ReadLine();
            var number = 0;
            try
            {
                if (numberStr.Length == 0) throw new Exception("Incorrect number");
                number = Convert.ToInt32(numberStr, 2);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Invalid data! Error - {e.Message}");
                return;
            }

            var numberLen = (sbyte) numberStr.Length;
            Console.WriteLine($"Введенное число: {number}");

            // 1, 2
            Console.WriteLine();
            Console.WriteLine("Введите количество битов:");
            var bitNumberStr = Console.ReadLine();
            if (!sbyte.TryParse(bitNumberStr, out var bitNumber))
            {
                Console.WriteLine("Invalid data!");
                return;
            }

            Console.WriteLine($"1. \"Склеенное\" число - {ConnectParts(number, numberLen, bitNumber)}");
            Console.WriteLine($"2. \"Остальная\" часть - {GetBitsBetween(number, numberLen, bitNumber)}");
        }
    }
}