﻿using System;

namespace CryptographyLab1.Numbers
{
    public class Number4
    {
        public static int Max2Pow(int number)
        {
            return (int) Math.Log(number & -number, 2);
        }

        public static void Run()
        {
            // Main
            Console.WriteLine("Введите 32-х разрядное целое число 𝑎 в двоичной системе счисления");
            var numberStr = Console.ReadLine();
            var number = 0;
            try
            {
                if (string.IsNullOrEmpty(numberStr)) throw new Exception("Incorrect number");
                number = Convert.ToInt32(numberStr, 2);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Invalid data! Error - {e.Message}");
                return;
            }

            Console.WriteLine($"Введенное число: {number}");

            Console.WriteLine();
            Console.WriteLine($"Максимальная степень 2, на которую делится данное целое число - {Max2Pow(number)}");
        }
    }
}