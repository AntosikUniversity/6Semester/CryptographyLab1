﻿using System;

namespace CryptographyLab1.Numbers
{
    public static class Number7
    {
        public static int CircularShiftLeft(int number, sbyte byteCount)
        {
            var numberIn2 = Convert.ToString(number, 2);
            var bitsCount = numberIn2.Length;

            return (number << byteCount) | (number >> (bitsCount - byteCount));
        }

        public static int CircularShiftRight(int number, sbyte byteCount)
        {
            var numberIn2 = Convert.ToString(number, 2);
            var bitsCount = numberIn2.Length;

            return (number >> byteCount) | (number << (bitsCount - byteCount));
        }

        public static void Run()
        {
            // Main
            Console.WriteLine("Введите 32-х разрядное целое число 𝑎 в двоичной системе счисления");
            var numberStr = Console.ReadLine();
            var number = 0;
            try
            {
                if (string.IsNullOrEmpty(numberStr)) throw new Exception("Incorrect number");
                number = Convert.ToInt32(numberStr, 2);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Invalid data! Error - {e.Message}");
                return;
            }

            Console.WriteLine($"Введенное число: {number}");

            Console.WriteLine();
            Console.WriteLine("Введите число, на которое нужно сделать сдвиг бит:");
            var shiftNumberStr = Console.ReadLine();
            if (!sbyte.TryParse(shiftNumberStr, out var shiftNumber))
            {
                Console.WriteLine("Invalid data!");
                return;
            }

            Console.WriteLine();
            Console.WriteLine(
                $"После циклического сдвига влево на {shiftNumber} - {CircularShiftLeft(number, shiftNumber)}");
            Console.WriteLine(
                $"После циклического сдвига вправо на {shiftNumber} - {CircularShiftRight(number, shiftNumber)}");
        }
    }
}