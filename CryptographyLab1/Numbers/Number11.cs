﻿using System;
using System.Linq;

namespace CryptographyLab1.Numbers
{
    public static class Number11
    {
        public static void Run()
        {
            // Main
            Console.WriteLine($"Шифр Вижинера:");
            var str = "Relative frequencies of letters in the English language";
            Console.WriteLine($"Начальный текст - {str}");
            var key = "asdf";
            Console.WriteLine($"Ключ: {key}");
            var encodedWithViginere = VigenereCipher.Encrypt(str, key);
            Console.WriteLine($"Зашифрованный текст - {encodedWithViginere}");
            Console.WriteLine();
            var hackedShift = ShiftCipher.Hack(str);
            Console.WriteLine($"Взломанный текст - {hackedShift}");
        }
    }

    public static partial class VigenereCipher
    {
        public static string Hack(string data, int keyLength)
        {
            var key = new char[keyLength];
            for (var i = 0; i < keyLength; i++)
            {
                var mostFrequentChar = data
                    .Where((ch, index) => index % keyLength == i)
                    .GroupBy(ch => ch)
                    .OrderByDescending(group => group.Count())
                    .First()
                    .Key;
                var topFrequnecyChar = Helpers.GetCharInLetterFrequnecyPos(1);
                key[i] = (char) (topFrequnecyChar - mostFrequentChar);
            }

            return Decrypt(data, new string(key));
        }
    }
}