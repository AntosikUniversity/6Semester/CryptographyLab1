﻿using System;
using System.Collections;

namespace CryptographyLab1.Numbers
{
    public static class Number6
    {
        // TODO: rework
        public static int XorAllBits(int number, sbyte p)
        {
            var bitArray = new BitArray(new[] {number});
            var result = bitArray[p];
            for (var i = p - 1; i >= 0; i--) result ^= bitArray[i];
            return result ? 1 : 0;
        }

        public static void Run()
        {
            // Main
            Console.WriteLine("Введите 32-х разрядное целое число 𝑎 в двоичной системе счисления");
            var numberStr = Console.ReadLine();
            var number = 0;
            try
            {
                if (string.IsNullOrEmpty(numberStr)) throw new Exception("Incorrect number");
                number = Convert.ToInt32(numberStr, 2);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Invalid data! Error - {e.Message}");
                return;
            }

            var numberLen = (sbyte) numberStr.Length;

            Console.WriteLine($"Введенное число: {number}");

            Console.WriteLine();
            Console.WriteLine($"Результать XOR'a всех битов числа - {XorAllBits(number, numberLen)}");
        }
    }
}