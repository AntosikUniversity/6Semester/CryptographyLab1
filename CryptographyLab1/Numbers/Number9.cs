﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CryptographyLab1.Numbers
{
    public static class Number9
    {
        public static void Run()
        {
            // Main
            Console.WriteLine("Начальный текст - Hello World");
            var str = "Hello World";

            Console.WriteLine();
            Console.WriteLine($"Шифр сдвига: (ключ - 3)");
            var encodedWithShift = ShiftCipher.Encrypt(str, 3);
            Console.WriteLine($"Зашифрованный текст - {encodedWithShift}");
            Console.WriteLine($"Дешифрованный текст - {ShiftCipher.Decrypt(encodedWithShift, 3)}");

            Console.WriteLine();
            Console.WriteLine($"Шифр замены:");
            Console.WriteLine($"Ключ: h → z, e → y, l → x, o → m, w → a, r → b, d → c");
            var replacementDictionary = new Dictionary<char, char>
            {
                {'h', 'z'},
                {'e', 'y'},
                {'l', 'x'},
                {'o', 'm'},
                {'w', 'a'},
                {'r', 'b'},
                {'d', 'c'}
            };
            var encodedWithReplacement = ReplacementCipher.Encrypt(str, replacementDictionary);
            Console.WriteLine($"Зашифрованный текст - {encodedWithReplacement}");
            Console.WriteLine(
                $"Дешифрованный текст - {ReplacementCipher.Decrypt(encodedWithReplacement, replacementDictionary)}");

            Console.WriteLine();
            Console.WriteLine($"Шифр Вижинера:");
            var viginereKey1 = "asdf";
            var viginereKey2 = "zxcv";
            Console.WriteLine($"Ключ 1: {viginereKey1}");
            Console.WriteLine($"Ключ 2: {viginereKey2}");
            var encodedWithViginere = VigenereCipher.Encrypt(str, viginereKey1, viginereKey2);
            Console.WriteLine($"Зашифрованный текст - {encodedWithViginere}");
            Console.WriteLine(
                $"Дешифрованный текст - {VigenereCipher.Decrypt(encodedWithViginere, viginereKey1, viginereKey2)}");
        }
    }

    public static partial class ShiftCipher
    {
        public static string Encrypt(string data, int key)
        {
            key = Math.Abs(key % 26);
            data = data.ToLower();

            if (key == 0) return data;


            return new string(
                data.Select(ch =>
                    char.IsLetter(ch) ? (char) ((ch - 'a' + key) % 26 + 'a') : ch
                ).ToArray()
            );
        }

        public static string Decrypt(string data, int key)
        {
            key = Math.Abs(key % 26);
            data = data.ToLower();

            if (key == 0) return data;


            return new string(
                data.Select(ch =>
                    char.IsLetter(ch) ? (char) ((ch - 'z' - key) % 26 + 'z') : ch
                ).ToArray()
            );
        }
    }

    public static class ReplacementCipher
    {
        public static string Encrypt(string data, Dictionary<char, char> key)
        {
            data = data.ToLower();

            return new string(data.Select(ch =>
                key.ContainsKey(ch) ? key[ch] : '?'
            ).ToArray());
        }

        public static string Decrypt(string data, Dictionary<char, char> key)
        {
            data = data.ToLower();

            return new string(data.Select(ch =>
                key.ContainsValue(ch) ? key.FirstOrDefault(x => x.Value == ch).Key : '?'
            ).ToArray());
        }
    }

    public static partial class VigenereCipher
    {
        public static string Encrypt(string data, params string[] keys)
        {
            data = data.ToLower();
            var result = data;

            foreach (var key in keys)
            {
                var keyLen = key.Length;
                var keyL = key.ToLower();

                result = new string(result.Select((ch, i) =>
                    char.IsLetter(ch) ? (char) ((ch + keyL[i % keyLen] - 2 * 'a') % 26 + 'a') : ch
                ).ToArray());
            }

            return result;
        }

        public static string Decrypt(string data, params string[] keys)
        {
            data = data.ToLower();
            keys = keys.Reverse().ToArray();
            var result = data;

            foreach (var key in keys)
            {
                var keyLen = key.Length;
                var keyL = key.ToLower();

                result = new string(result.Select((ch, i) =>
                    char.IsLetter(ch) ? (char) ((ch - keyL[i % keyLen] + 'a' - 'z') % 26 + 'z') : ch
                ).ToArray());
            }

            return result;
        }
    }
}